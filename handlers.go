package main

import (
	pb "bitbucket.com/go/calculatorservicemodel"
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	"html"
	"net/http"
	"strconv"
)

// Return default message for root routing
func indexHTTP(w http.ResponseWriter, r *http.Request) {
	log.Debug().Msg("Got root")
	fmt.Fprintf(w, "Hello %q", html.EscapeString(r.URL.Path))
}

// Return echo message
//func echoHTTPHandler(pool *grpcpool.Pool) func(w http.ResponseWriter, r *http.Request) {
func echoHTTPHandler(conn *grpc.ClientConn) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		params := mux.Vars(r)

		// Creates a new client from pool
		/*ctx := r.Context()
		conn, err := pool.Get(ctx)
		defer conn.Close()
		if err != nil {
			log.Error().Msgf("%s", err.Error())
			fmt.Fprintf(w, "%s", err.Error())
			return
		}*/

		// Creates a new CustomerClient
		sc := pb.NewCalculatorServiceClient(conn)

		msg := &pb.EchoRequest{
			Message: params["message"],
		}

		resp, error := sc.Echo(context.Background(), msg)
		if error != nil {
			log.Error().Msgf("Error: ", error.Error())
			fmt.Fprintf(w, "%s", error.Error())
			return
		}

		fmt.Fprintf(w, "%s", resp.Message)
	}

}

// Return factorial iterative
//func factorialIterativeHandler(pool *grpcpool.Pool) func(w http.ResponseWriter, r *http.Request) {
func factorialIterativeHandler(conn *grpc.ClientConn) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		params := mux.Vars(r)

		numberParam, err := strconv.ParseInt(params["number"], 10, 32)
		if err != nil {
			log.Fatal().Msgf("Error parsing parameter: %v", err.Error())
			fmt.Fprintf(w, "Error parsing parameter: %v", err.Error())
			return
		}

		// Creates a new client
		/*ctx := r.Context()
		conn, err := pool.Get(ctx)
		defer conn.Close()
		if err != nil {
			log.Error().Msgf("%s", err.Error())
			fmt.Fprintf(w, "%s", err.Error())
			return
		}*/

		number := &pb.FactorialRequest{
			Number: int32(numberParam),
		}

		sc := pb.NewCalculatorServiceClient(conn)

		resp, error := sc.FactorialIterative(context.Background(), number)
		if error != nil {
			log.Error().Msgf("Error: ", error.Error())
			fmt.Fprintf(w, "%s", error.Error())
		}

		fmt.Fprintf(w, "%s", resp.GetFactorial())
	}
}
