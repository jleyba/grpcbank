normal: clean
	go build

# Generate code for linux
linux: clean
	env GOOS=linux GOARCH=amd64 go build
	zip -9 grpcbank_linux_v0.1.zip grpcbank conf.yaml

# Generate code for windows
windows:
	env GOOS=windows GOARCH=amd64 go build
	mv grpcbank.exe grpcbank_amd64.exe
	zip -9 grpcbank_win_v0.1.zip grpcbank_amd64.exe conf.yaml

# Clean all old files
clean:
	rm -f grpcbank
	rm -f *.exe

