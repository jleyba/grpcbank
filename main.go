package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"
)

// Display start messages
func start() {

	fmt.Println("Starting server")

	// Start to read conf file
	fmt.Print("\n\n")
	fmt.Println("=============================================")
	fmt.Println("    Configuration checking - gprcBank v0.1")
	fmt.Println("=============================================")

	// loading configuration
	viper.SetConfigName("conf")                                   // name of config file (without ext)
	viper.AddConfigPath(".")                                      // default path for conf file
	viper.SetDefault("port", ":9296")                             // default port value
	viper.SetDefault("calledServiceURL", "http://localhost:9596") // default calledServiceURL
	viper.SetDefault("loglevel", "info")                          // default port value

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		fmt.Printf("Fatal error config file: %v \n", err)
		panic(err)
	}

	fmt.Println("-- Using port:              ", viper.GetString("port"))

	zerolog.TimeFieldFormat = time.RFC3339
	zerolog.TimestampFieldName = "@timestamp"
	switch viper.GetString("loglevel") {
	case "info":
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case "error":
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "disabled":
		zerolog.SetGlobalLevel(zerolog.Disabled)
	default:
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	fmt.Println("-- Log lever set to:        ", viper.GetString("loglevel"))
	fmt.Println("-- CalledServiceURL set to: ", viper.GetString("calledServiceURL"))

	fmt.Println("=============================================")

}

func main() {

	start()
	// CPU profiling by default
	//defer profile.Start().Stop()
	// Memory profiling
	//defer profile.Start(profile.MemProfile).Stop()

	/*var factory grpcpool.Factory

	factory = func() (*grpc.ClientConn, error) {
		conn, err := grpc.Dial(viper.GetString("calledServiceURL"), grpc.WithInsecure())
		if err != nil {
			log.Fatal().Msgf("Failed to start gRPC connection: %v", err.Error())
		}
		log.Debug().Msgf("Connected to server at %s", viper.GetString("calledServiceURL"))
		return conn, err
	}

	pool, err := grpcpool.New(factory, 100, 600, time.Second * 1)
	if err != nil {
		log.Error().Msgf("Failed to create gRPC pool: %v", err.Error())
	}*/

	// Set up a connection to the gRPC server.
	conn, err := grpc.Dial(viper.GetString("calledServiceURL"), grpc.WithInsecure())
	if err != nil {
		log.Fatal().Msgf("did not connect: %v", err.Error())
		os.Exit(1)
	}
	//defer conn.Close()

	// Set the muxer
	r := mux.NewRouter()
	r.HandleFunc("/", indexHTTP)
	r.HandleFunc("/echo/{message}", echoHTTPHandler(conn))
	r.HandleFunc("/factorialIterative/{number}", factorialIterativeHandler(conn))

	// Set server options
	srv := &http.Server{
		Addr:           viper.GetString("port"),
		Handler:        r,
		ReadTimeout:    2 * time.Second,
		MaxHeaderBytes: 1 << 20,
		IdleTimeout:    time.Second * 2,
	}

	// Add connection State hook for tracing errors.
	srv.ConnState = func(c net.Conn, cs http.ConnState) {
		switch cs {
		case http.StateIdle:
			c.SetReadDeadline(time.Now().Add(time.Second * 2))
			log.Debug().Msgf("StateIddle: %s", c.LocalAddr().String())
		case http.StateNew:
			c.SetReadDeadline(time.Now().Add(time.Second * 2))
			log.Debug().Msgf("StateNew: %s", c.LocalAddr().String())
		case http.StateActive:
			log.Debug().Msgf("Active: %s", c.LocalAddr().String())
			c.SetReadDeadline(time.Now().Add(time.Second * 2))
		}
	}

	// Lanuch server in a thread
	go func() {
		fmt.Println("Starting server...")
		log.Info().Msg("Starting server...")
		if err := srv.ListenAndServe(); err != nil {
			log.Panic().Msgf("%s", err)
		}
	}()

	// Process a graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	defer conn.Close()
	errShutdown := srv.Shutdown(ctx)
	if errShutdown != nil {
		panic(fmt.Sprintf("Error shutting down %s", errShutdown))
	}

	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	fmt.Print("\n\n")
	fmt.Println("shutting down")
	fmt.Println("Goddbye!....")
	os.Exit(0)

}
